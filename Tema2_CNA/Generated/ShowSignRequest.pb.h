// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ShowSignRequest.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_ShowSignRequest_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_ShowSignRequest_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3011000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3011003 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_ShowSignRequest_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_ShowSignRequest_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[1]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_ShowSignRequest_2eproto;
class ShowSignRequest;
class ShowSignRequestDefaultTypeInternal;
extern ShowSignRequestDefaultTypeInternal _ShowSignRequest_default_instance_;
PROTOBUF_NAMESPACE_OPEN
template<> ::ShowSignRequest* Arena::CreateMaybeMessage<::ShowSignRequest>(Arena*);
PROTOBUF_NAMESPACE_CLOSE

// ===================================================================

class ShowSignRequest :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:ShowSignRequest) */ {
 public:
  ShowSignRequest();
  virtual ~ShowSignRequest();

  ShowSignRequest(const ShowSignRequest& from);
  ShowSignRequest(ShowSignRequest&& from) noexcept
    : ShowSignRequest() {
    *this = ::std::move(from);
  }

  inline ShowSignRequest& operator=(const ShowSignRequest& from) {
    CopyFrom(from);
    return *this;
  }
  inline ShowSignRequest& operator=(ShowSignRequest&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const ShowSignRequest& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const ShowSignRequest* internal_default_instance() {
    return reinterpret_cast<const ShowSignRequest*>(
               &_ShowSignRequest_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(ShowSignRequest& a, ShowSignRequest& b) {
    a.Swap(&b);
  }
  inline void Swap(ShowSignRequest* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline ShowSignRequest* New() const final {
    return CreateMaybeMessage<ShowSignRequest>(nullptr);
  }

  ShowSignRequest* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<ShowSignRequest>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const ShowSignRequest& from);
  void MergeFrom(const ShowSignRequest& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  ::PROTOBUF_NAMESPACE_ID::uint8* _InternalSerialize(
      ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(ShowSignRequest* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "ShowSignRequest";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_ShowSignRequest_2eproto);
    return ::descriptor_table_ShowSignRequest_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kSignBirthdayFieldNumber = 1,
  };
  // string SignBirthday = 1;
  void clear_signbirthday();
  const std::string& signbirthday() const;
  void set_signbirthday(const std::string& value);
  void set_signbirthday(std::string&& value);
  void set_signbirthday(const char* value);
  void set_signbirthday(const char* value, size_t size);
  std::string* mutable_signbirthday();
  std::string* release_signbirthday();
  void set_allocated_signbirthday(std::string* signbirthday);
  private:
  const std::string& _internal_signbirthday() const;
  void _internal_set_signbirthday(const std::string& value);
  std::string* _internal_mutable_signbirthday();
  public:

  // @@protoc_insertion_point(class_scope:ShowSignRequest)
 private:
  class _Internal;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr signbirthday_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  friend struct ::TableStruct_ShowSignRequest_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// ShowSignRequest

// string SignBirthday = 1;
inline void ShowSignRequest::clear_signbirthday() {
  signbirthday_.ClearToEmptyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline const std::string& ShowSignRequest::signbirthday() const {
  // @@protoc_insertion_point(field_get:ShowSignRequest.SignBirthday)
  return _internal_signbirthday();
}
inline void ShowSignRequest::set_signbirthday(const std::string& value) {
  _internal_set_signbirthday(value);
  // @@protoc_insertion_point(field_set:ShowSignRequest.SignBirthday)
}
inline std::string* ShowSignRequest::mutable_signbirthday() {
  // @@protoc_insertion_point(field_mutable:ShowSignRequest.SignBirthday)
  return _internal_mutable_signbirthday();
}
inline const std::string& ShowSignRequest::_internal_signbirthday() const {
  return signbirthday_.GetNoArena();
}
inline void ShowSignRequest::_internal_set_signbirthday(const std::string& value) {
  
  signbirthday_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), value);
}
inline void ShowSignRequest::set_signbirthday(std::string&& value) {
  
  signbirthday_.SetNoArena(
    &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:ShowSignRequest.SignBirthday)
}
inline void ShowSignRequest::set_signbirthday(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  
  signbirthday_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:ShowSignRequest.SignBirthday)
}
inline void ShowSignRequest::set_signbirthday(const char* value, size_t size) {
  
  signbirthday_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:ShowSignRequest.SignBirthday)
}
inline std::string* ShowSignRequest::_internal_mutable_signbirthday() {
  
  return signbirthday_.MutableNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline std::string* ShowSignRequest::release_signbirthday() {
  // @@protoc_insertion_point(field_release:ShowSignRequest.SignBirthday)
  
  return signbirthday_.ReleaseNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline void ShowSignRequest::set_allocated_signbirthday(std::string* signbirthday) {
  if (signbirthday != nullptr) {
    
  } else {
    
  }
  signbirthday_.SetAllocatedNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), signbirthday);
  // @@protoc_insertion_point(field_set_allocated:ShowSignRequest.SignBirthday)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)


// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_ShowSignRequest_2eproto
