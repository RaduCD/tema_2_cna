// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: ShowSignOperation.proto

#include "ShowSignOperation.pb.h"
#include "ShowSignOperation.grpc.pb.h"

#include <functional>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>
#include <grpcpp/impl/codegen/channel_interface.h>
#include <grpcpp/impl/codegen/client_unary_call.h>
#include <grpcpp/impl/codegen/client_callback.h>
#include <grpcpp/impl/codegen/message_allocator.h>
#include <grpcpp/impl/codegen/method_handler.h>
#include <grpcpp/impl/codegen/rpc_service_method.h>
#include <grpcpp/impl/codegen/server_callback.h>
#include <grpcpp/impl/codegen/server_callback_handlers.h>
#include <grpcpp/impl/codegen/server_context.h>
#include <grpcpp/impl/codegen/service_type.h>
#include <grpcpp/impl/codegen/sync_stream.h>

static const char* ShowSignOperationService_method_names[] = {
  "/ShowSignOperationService/ShowTheSign",
};

std::unique_ptr< ShowSignOperationService::Stub> ShowSignOperationService::NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options) {
  (void)options;
  std::unique_ptr< ShowSignOperationService::Stub> stub(new ShowSignOperationService::Stub(channel));
  return stub;
}

ShowSignOperationService::Stub::Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel)
  : channel_(channel), rpcmethod_ShowTheSign_(ShowSignOperationService_method_names[0], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  {}

::grpc::Status ShowSignOperationService::Stub::ShowTheSign(::grpc::ClientContext* context, const ::ShowSignRequest& request, ::ShowSign* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_ShowTheSign_, context, request, response);
}

void ShowSignOperationService::Stub::experimental_async::ShowTheSign(::grpc::ClientContext* context, const ::ShowSignRequest* request, ::ShowSign* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_ShowTheSign_, context, request, response, std::move(f));
}

void ShowSignOperationService::Stub::experimental_async::ShowTheSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::ShowSign* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_ShowTheSign_, context, request, response, std::move(f));
}

void ShowSignOperationService::Stub::experimental_async::ShowTheSign(::grpc::ClientContext* context, const ::ShowSignRequest* request, ::ShowSign* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_ShowTheSign_, context, request, response, reactor);
}

void ShowSignOperationService::Stub::experimental_async::ShowTheSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::ShowSign* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_ShowTheSign_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::ShowSign>* ShowSignOperationService::Stub::AsyncShowTheSignRaw(::grpc::ClientContext* context, const ::ShowSignRequest& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::ShowSign>::Create(channel_.get(), cq, rpcmethod_ShowTheSign_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::ShowSign>* ShowSignOperationService::Stub::PrepareAsyncShowTheSignRaw(::grpc::ClientContext* context, const ::ShowSignRequest& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::ShowSign>::Create(channel_.get(), cq, rpcmethod_ShowTheSign_, context, request, false);
}

ShowSignOperationService::Service::Service() {
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      ShowSignOperationService_method_names[0],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< ShowSignOperationService::Service, ::ShowSignRequest, ::ShowSign>(
          std::mem_fn(&ShowSignOperationService::Service::ShowTheSign), this)));
}

ShowSignOperationService::Service::~Service() {
}

::grpc::Status ShowSignOperationService::Service::ShowTheSign(::grpc::ServerContext* context, const ::ShowSignRequest* request, ::ShowSign* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}


