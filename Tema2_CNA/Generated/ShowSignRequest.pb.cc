// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ShowSignRequest.proto

#include "ShowSignRequest.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
class ShowSignRequestDefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<ShowSignRequest> _instance;
} _ShowSignRequest_default_instance_;
static void InitDefaultsscc_info_ShowSignRequest_ShowSignRequest_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::_ShowSignRequest_default_instance_;
    new (ptr) ::ShowSignRequest();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::ShowSignRequest::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<0> scc_info_ShowSignRequest_ShowSignRequest_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 0, 0, InitDefaultsscc_info_ShowSignRequest_ShowSignRequest_2eproto}, {}};

static ::PROTOBUF_NAMESPACE_ID::Metadata file_level_metadata_ShowSignRequest_2eproto[1];
static constexpr ::PROTOBUF_NAMESPACE_ID::EnumDescriptor const** file_level_enum_descriptors_ShowSignRequest_2eproto = nullptr;
static constexpr ::PROTOBUF_NAMESPACE_ID::ServiceDescriptor const** file_level_service_descriptors_ShowSignRequest_2eproto = nullptr;

const ::PROTOBUF_NAMESPACE_ID::uint32 TableStruct_ShowSignRequest_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  ~0u,  // no _has_bits_
  PROTOBUF_FIELD_OFFSET(::ShowSignRequest, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  PROTOBUF_FIELD_OFFSET(::ShowSignRequest, signbirthday_),
};
static const ::PROTOBUF_NAMESPACE_ID::internal::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, -1, sizeof(::ShowSignRequest)},
};

static ::PROTOBUF_NAMESPACE_ID::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::_ShowSignRequest_default_instance_),
};

const char descriptor_table_protodef_ShowSignRequest_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n\025ShowSignRequest.proto\"\'\n\017ShowSignReque"
  "st\022\024\n\014SignBirthday\030\001 \001(\tB\014\252\002\tGeneratedb\006"
  "proto3"
  ;
static const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable*const descriptor_table_ShowSignRequest_2eproto_deps[1] = {
};
static ::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase*const descriptor_table_ShowSignRequest_2eproto_sccs[1] = {
  &scc_info_ShowSignRequest_ShowSignRequest_2eproto.base,
};
static ::PROTOBUF_NAMESPACE_ID::internal::once_flag descriptor_table_ShowSignRequest_2eproto_once;
static bool descriptor_table_ShowSignRequest_2eproto_initialized = false;
const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_ShowSignRequest_2eproto = {
  &descriptor_table_ShowSignRequest_2eproto_initialized, descriptor_table_protodef_ShowSignRequest_2eproto, "ShowSignRequest.proto", 86,
  &descriptor_table_ShowSignRequest_2eproto_once, descriptor_table_ShowSignRequest_2eproto_sccs, descriptor_table_ShowSignRequest_2eproto_deps, 1, 0,
  schemas, file_default_instances, TableStruct_ShowSignRequest_2eproto::offsets,
  file_level_metadata_ShowSignRequest_2eproto, 1, file_level_enum_descriptors_ShowSignRequest_2eproto, file_level_service_descriptors_ShowSignRequest_2eproto,
};

// Force running AddDescriptors() at dynamic initialization time.
static bool dynamic_init_dummy_ShowSignRequest_2eproto = (  ::PROTOBUF_NAMESPACE_ID::internal::AddDescriptors(&descriptor_table_ShowSignRequest_2eproto), true);

// ===================================================================

void ShowSignRequest::InitAsDefaultInstance() {
}
class ShowSignRequest::_Internal {
 public:
};

ShowSignRequest::ShowSignRequest()
  : ::PROTOBUF_NAMESPACE_ID::Message(), _internal_metadata_(nullptr) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:ShowSignRequest)
}
ShowSignRequest::ShowSignRequest(const ShowSignRequest& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _internal_metadata_(nullptr) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  signbirthday_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  if (!from._internal_signbirthday().empty()) {
    signbirthday_.AssignWithDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from.signbirthday_);
  }
  // @@protoc_insertion_point(copy_constructor:ShowSignRequest)
}

void ShowSignRequest::SharedCtor() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&scc_info_ShowSignRequest_ShowSignRequest_2eproto.base);
  signbirthday_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}

ShowSignRequest::~ShowSignRequest() {
  // @@protoc_insertion_point(destructor:ShowSignRequest)
  SharedDtor();
}

void ShowSignRequest::SharedDtor() {
  signbirthday_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}

void ShowSignRequest::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const ShowSignRequest& ShowSignRequest::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_ShowSignRequest_ShowSignRequest_2eproto.base);
  return *internal_default_instance();
}


void ShowSignRequest::Clear() {
// @@protoc_insertion_point(message_clear_start:ShowSignRequest)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  signbirthday_.ClearToEmptyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  _internal_metadata_.Clear();
}

const char* ShowSignRequest::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // string SignBirthday = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 10)) {
          auto str = _internal_mutable_signbirthday();
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(::PROTOBUF_NAMESPACE_ID::internal::VerifyUTF8(str, "ShowSignRequest.SignBirthday"));
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag, &_internal_metadata_, ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}

::PROTOBUF_NAMESPACE_ID::uint8* ShowSignRequest::_InternalSerialize(
    ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:ShowSignRequest)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // string SignBirthday = 1;
  if (this->signbirthday().size() > 0) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_signbirthday().data(), static_cast<int>(this->_internal_signbirthday().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "ShowSignRequest.SignBirthday");
    target = stream->WriteStringMaybeAliased(
        1, this->_internal_signbirthday(), target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields(), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:ShowSignRequest)
  return target;
}

size_t ShowSignRequest::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:ShowSignRequest)
  size_t total_size = 0;

  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // string SignBirthday = 1;
  if (this->signbirthday().size() > 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_signbirthday());
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    return ::PROTOBUF_NAMESPACE_ID::internal::ComputeUnknownFieldsSize(
        _internal_metadata_, total_size, &_cached_size_);
  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void ShowSignRequest::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:ShowSignRequest)
  GOOGLE_DCHECK_NE(&from, this);
  const ShowSignRequest* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<ShowSignRequest>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:ShowSignRequest)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:ShowSignRequest)
    MergeFrom(*source);
  }
}

void ShowSignRequest::MergeFrom(const ShowSignRequest& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:ShowSignRequest)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  if (from.signbirthday().size() > 0) {

    signbirthday_.AssignWithDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from.signbirthday_);
  }
}

void ShowSignRequest::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:ShowSignRequest)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void ShowSignRequest::CopyFrom(const ShowSignRequest& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:ShowSignRequest)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool ShowSignRequest::IsInitialized() const {
  return true;
}

void ShowSignRequest::InternalSwap(ShowSignRequest* other) {
  using std::swap;
  _internal_metadata_.Swap(&other->_internal_metadata_);
  signbirthday_.Swap(&other->signbirthday_, &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
    GetArenaNoVirtual());
}

::PROTOBUF_NAMESPACE_ID::Metadata ShowSignRequest::GetMetadata() const {
  return GetMetadataStatic();
}


// @@protoc_insertion_point(namespace_scope)
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::ShowSignRequest* Arena::CreateMaybeMessage< ::ShowSignRequest >(Arena* arena) {
  return Arena::CreateInternal< ::ShowSignRequest >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
