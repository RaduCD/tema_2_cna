#include <iostream>
#include <ShowSignOperation.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
    grpc_init();
    ClientContext context;
    auto sum_stub = ShowSignOperationService::NewStub(grpc::CreateChannel("localhost:1234",
        grpc::InsecureChannelCredentials()));
    ShowSignRequest nameRequest;
    std::cout << "Insert your birthDate here:";

    std::string birthday;

    std::cin >> birthday;

    nameRequest.set_signbirthday(birthday);
    ShowSign response;
    auto status = sum_stub->ShowTheSign(&context, nameRequest, &response);

    if (status.ok())
    {
        std::cout << "Your sign is: " << response.sign() << '\n';
    }
}