#pragma once
#include<string>
#include<vector>
#include<fstream>
#include"Sign.h"
#include"Date.h"

class ParseFromFile
{
private:
	ParseFromFile() = default;
	std::vector<Sign*> listSigns;
	static ParseFromFile* instance;
	std::vector<int> limitsForEachMounths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

public:
	static ParseFromFile* getInstance();
	std::vector<Sign*> initListSign();
	std::string findTheSignFromDate(Date* requestDate);
};

